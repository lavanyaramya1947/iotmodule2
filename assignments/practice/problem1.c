/* @file problem1.c
   @brief code to send heart beat messages on MQTT topic

   @author Lavanya T
   */


/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *  This block contains initialization code for this particular file.
 *#####################################################################
 */

 /*-----Standard Includes-----*/

#include  <stdio.h>//for standard input and output
#include <string.h>//for manipulating C strings and arrays
#include <stdlib.h>//for functions involving memory allocation,process control,conversions and others

/*-----Project Includes-----*/
#include <MQTTClient.h>//file contains APIs to send/receive MQTT data
#include<time.h>//to get and manipulate date and time information
#include<netdb.h>//for network database operations
#include<ifaddrs.h>//file for getting network interface adderesses
#include <si/shunyaInterfaces.h>// to program embedded boards to talk to PLCs,sensors,actuators,cloud.

//fetching device id
void getdeviceID() {
    //variable declaration
    char device_id[255];
    //file pointer to access the file
    FILE *fp;
    // open file to read 
    fp = fopen("/etc/shunya/deviceid" ,"r"); 
   
    while (fscanf(fp,"%s",device_id)!= EOF)
    {
       //print device id
       printf("%s",device_id)
    }
    
   
}

// Fetching timestamp
int TimeStamp() {
     int t_stamp;//variable declaration
     t_stamp = (gettimeofday(2));// to get the time
     return t_stamp;//return the value
}

function getIPAdress(char *IP) {

 char *IP;//pointer
 IP = inet_ntoa(*((struct in_addr*)host_entry-->h_addr_list[0]));//value & conversion

 return 0;

}

//configuration of connections with AWS IOT core
void AWS_config(void){
"user":{
"endpoint": " ",
"port":8883,
"certificate dir":"/home/shunya/.cert/aws",
"root certificate":" ",
"client certificate":" ",
"private key":" ",
"client ID":" "
}
}

void get_Data(){
MQTTClient_message heartbeat = MQTTClient_message_intializer;//intializes message to send on broker
awsObj user = newAws("user");
awsConnectMqtt(&user);//connects to MQTT broker of AWS IOT core
heartbeat.payload("device":{
 "deviceId": id,
 "timestamp": ts,
 "eventType": "heartbeat",
 "ipAddress": IP
}.getBytes());//creates payload of message
awsPublishMqtt(&user, "device/heartbeat", "%s" ,heartbeat);
awsDisconnectMqtt(&user);
}


int  main(void) //main function
{
    //initLib();//intializing library
    shunyaInterface();// calling shunya interface
     getDeviceID();//calling device id function to fetch id
     getTimeStamp();// calling time stamp function
     getIPAdress();//calling IP address function to fetch IP address
     get_Data();
     AWS_config(void);
    
    
    return 0;

}




	



